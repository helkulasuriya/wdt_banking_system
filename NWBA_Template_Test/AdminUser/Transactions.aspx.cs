﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using NWBA_Template_Test.App_Code;
using System.Data.SqlClient;

namespace NWBA_Template_Test.AdminUser
{
    public partial class Transactions : System.Web.UI.Page
    {
        private ITransactionFacade transactionFacade = new TransactionFacade();
        private static decimal accntBalance = 0.0M;
        private static Int32 accNum = 0;
        static List<Transaction> transactionsList = null;

        public decimal AccntBalance { get { return accntBalance; } }

        protected void Page_Load(object sender, EventArgs e)
        {
            //if (Session.IsNewSession || ((Session.Keys == null) || (Session.Keys.Count == 0)))
            //    Response.Redirect("~/Auth/Login.aspx");
        }

        protected void selectAccountType_SelectedIndexChanged(object sender, EventArgs e)
        {
            String accountType = selectAccountType.SelectedValue;
            Int32 customerId = Convert.ToInt32(selectUser.SelectedValue);
            //Int32 customerId = 1;
            List<Account> accountsList = transactionFacade.getAccountsByUserandType(customerId, accountType);

            chooseAccount.Items.Clear();
            chooseAccount.Items.Add("");
            chooseAccount.SelectedIndex = 0;
            balanceAmtLabel.Text = "";

            if (!(accountsList == null) || !(accountsList.Count() == 0))
            {
                foreach (Account acc in accountsList)
                    chooseAccount.Items.Add(acc.AccountNumber.ToString());
            }
        }

        protected void chooseAccount_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                accNum = Int32.Parse(chooseAccount.SelectedValue);
            }
            catch (Exception argExc)
            {
                Trace.Write("\nException in parsing account number :  \n" + argExc.StackTrace);
            }
            // Setting value to the instance variable
            accntBalance = transactionFacade.showBalance(accNum);
            // Setting value to label.
            balanceAmtLabel.Text = accntBalance.ToString();
            // Making the history button visible
            viewAcntHistory.Visible = true;
        }

        protected void viewAcntHistory_Click(object sender, EventArgs e)
        {
            
                transactionsList = transactionFacade.showHistoryForAccountForAdmin(accNum);
                // Binding data to transaction grid.
                accountHistoryGrid.DataSource = transactionsList;
                accountHistoryGrid.DataBind();
            
        }

        protected void accountHistoryGrid_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            accountHistoryGrid.DataSource = transactionsList;
            accountHistoryGrid.PageIndex = e.NewPageIndex;
            accountHistoryGrid.DataBind();
        }
        protected void SelectedUser_DataBound(object sender, EventArgs e)
        {
            selectUser.Items.Insert(0, new ListItem("--Select User--", "-1"));
        }
    }
}