﻿<%@ Page Title="" Language="C#" MasterPageFile="~/AdminUser/Admin.Master" AutoEventWireup="true" CodeBehind="ScheduledPayments.aspx.cs" Inherits="NWBA_Template_Test.AdminUser.ScheduledPayments" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FeaturedContent" runat="server">
    Select a User<asp:DropDownList ID="SelectedUser" runat="server" AutoPostBack="True" DataSourceID="SelectUser" DataTextField="CustomerName" DataValueField="CustomerID" OnSelectedIndexChanged="SelectedUserChanged" OnDataBound="SelectedUser_DataBound">
        <asp:ListItem Selected="True" Value="-1">--Select User--</asp:ListItem>
    </asp:DropDownList>
    <asp:EntityDataSource ID="SelectUser" runat="server" ConnectionString="name=bankDB_WDT1234Entities" DefaultContainerName="bankDB_WDT1234Entities" EnableFlattening="False" EntitySetName="Customers" Select="it.[CustomerID], it.[CustomerName]">
    </asp:EntityDataSource>
    <asp:GridView ID="ScheduledGrid" runat="server" OnSelectedIndexChanged="revertScheduleSelected" OnRowDataBound="SelectColumns">
        <Columns>
            <asp:CommandField CancelText="Revert" HeaderText="Stop/Resume" InsertVisible="False" SelectText="Revert" ShowHeader="True" ShowSelectButton="True" />
        </Columns>
    </asp:GridView>
    <br />
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="MainContent" runat="server">
</asp:Content>
