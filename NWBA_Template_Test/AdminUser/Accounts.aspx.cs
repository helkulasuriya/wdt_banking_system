﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using NWBA_Template_Test.App_Code;

namespace NWBA_Template_Test.AdminUser
{
    public partial class Accounts : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            //Handle Session

            
        }

        protected void selectUser_SelectedIndexChanged(object sender, EventArgs e)
        {
            ITransactionFacade iuf = new TransactionFacade();
            accountsGrid.DataSource = iuf.getAccountsByCustId(Convert.ToInt32(selectUser.SelectedValue));            
            accountsGrid.DataBind();
        
        }

        protected void accountsGrid_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            e.Row.Cells[2].Visible = false;
            e.Row.Cells[3].Visible = false;
        }

        protected void SelectedUser_DataBound(object sender, EventArgs e)
        {
            selectUser.Items.Insert(0, new ListItem("--Select User--", "-1"));
        }
    }
}