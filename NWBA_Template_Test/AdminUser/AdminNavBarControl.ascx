﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="AdminNavBarControl.ascx.cs" Inherits="NWBA_Template_Test.AdminUser.AdminNavBarControl" %>
<header>
        <div class="menue_div_top">
            <table>
                <tbody>
                    <tr>
                        <td>
                            <img id="avc_logo_image" src="/Images/bankLogo2.png" width="130" height="80" style="padding-left: 50px; border: 0px" />
                        </td>
                        <td>
                            <div class="menue_div">
                                <ul class="Superfish menu">                                   
                                    <li class="selected current tab">
                                        <a class="menu_link" href="/AdminUser/Accounts.aspx">Accounts</a>
                                    </li>
                                    <li class="selected current tab">
                                        <a class="menu_link" href="/AdminUser/Transactions.aspx">Transactions</a>
                                    </li>
                                    <li class="selected current tab">
                                        <a class="menu_link" href="/AdminUser/ModifyDetails.aspx">Modify details</a>
                                    </li>
                                      <li class="selected current tab">
                                        <a class="menu_link" href="/AdminUser/ScheduledPayments.aspx">Scheduled payments</a>
                                    </li>
                                    <li class="selected current tab">
                                        <a class="menu_link" href="/Auth/Logout.aspx">Logout</a>
                                    </li>
                                </ul>
                            </div>
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
    </header>