﻿<%@ Page Title="" Language="C#" MasterPageFile="~/AdminUser/Admin.Master" AutoEventWireup="true" CodeBehind="Accounts.aspx.cs" Inherits="NWBA_Template_Test.AdminUser.Accounts" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FeaturedContent" runat="server">
    <p>
        <asp:Label ID="Label2" runat="server" Text="Select User" AssociatedControlID="selectUser"></asp:Label>
        <asp:DropDownList ID="selectUser" runat="server" OnSelectedIndexChanged="selectUser_SelectedIndexChanged" AutoPostBack="True" DataSourceID="EntityDataSource1" DataTextField="CustomerName" DataValueField="CustomerID" OnDataBound="SelectedUser_DataBound">
            <asp:ListItem Value="None" Selected="True">None..</asp:ListItem>
            
        </asp:DropDownList>
        <asp:EntityDataSource ID="EntityDataSource1" runat="server" ConnectionString="name=bankDB_WDT1234Entities" DefaultContainerName="bankDB_WDT1234Entities" EnableFlattening="False" EntitySetName="Customers" EntityTypeFilter="Customer" Select="it.[CustomerName], it.[CustomerID]">
        </asp:EntityDataSource>
        <br />
        <asp:GridView ID="accountsGrid" runat="server" OnRowDataBound="accountsGrid_RowDataBound">
        </asp:GridView>
        <br />

        
        <br />
    </p>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="MainContent" runat="server">
</asp:Content>
