﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using NWBA_Template_Test.App_Code;

namespace NWBA_Template_Test.AdminUser
{
    public partial class ScheduledPayments : System.Web.UI.Page
    {
        static List<AdminBPay> billPayList = null;
        IBPayFacade bpay = new BPayFacade();

        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void SelectedUserChanged(object sender, EventArgs e)
        {
            if (!SelectedUser.SelectedValue.Equals("-1"))
            {
                //Get the data and bind
                billPayList = bpay.getBillPaysWithPayeeName(Convert.ToInt32(SelectedUser.SelectedValue));
                
                ScheduledGrid.DataSource = billPayList;
                ScheduledGrid.DataBind();
            }
        }


        protected void revertScheduleSelected(object sender, EventArgs e)
        {
            //Handle the Stop/Resume here
            // Retrieving the selected bill pay ID and adding it to the session.
            Int32 selectedIndex = ScheduledGrid.SelectedRow.DataItemIndex;
            AdminBPay billPayObj = billPayList[selectedIndex];
            String status = String.Empty;

            //Check the status of the schedule and change to the other
            if (billPayObj.RecurringStatus.Equals("inactive"))
                status = "active";
            else if (billPayObj.RecurringStatus.Equals("active"))
                status = "inactive";

            if(!status.Equals(String.Empty))
                bpay.updateBPayStatusByID(billPayObj.BillPayID, status);

            billPayList = bpay.getBillPaysWithPayeeName(Convert.ToInt32(SelectedUser.SelectedValue));
                
            ScheduledGrid.DataSource = billPayList;
            ScheduledGrid.DataBind();

         //   Response.Redirect(Request.RawUrl);
        //    SelectedUser.SelectedIndex = selectedIndex;
        }

        protected void SelectColumns(object sender, GridViewRowEventArgs e)
        {
            e.Row.Cells[1].Visible = false;
            e.Row.Cells[7].Visible = false;

        }

        protected void SelectedUser_DataBound(object sender, EventArgs e)
        {
            SelectedUser.Items.Insert(0, new ListItem("--Select User--","-1"));
         
        }

    }
}