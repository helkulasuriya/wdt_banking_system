﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Logout.aspx.cs" Inherits="NWBA_Template_Test.Auth.Logout" MasterPageFile="~/Site.Master" %>

<asp:Content runat="server" ID="BodyContent" ContentPlaceHolderID="MainContent">

    <div id="content" style="float: left;">
        <hgroup>
            <h2>Logged out of the system.</h2>            
        </hgroup>
        <p>Thank you for using our service.</p>
    </div>
</asp:Content>
