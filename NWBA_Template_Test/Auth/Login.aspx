﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Login.aspx.cs" Inherits="NWBA_Template_Test.Auth.Login" MasterPageFile="~/Site.Master" %>

<asp:Content runat="server" ID="BodyContent" ContentPlaceHolderID="MainContent">
    <hgroup class="title">
        <h1><%: Title %>.</h1>
    </hgroup>
    <section id="loginForm">
        <h2>Use a local account to log in.</h2>
        <asp:Login ID="Login1" runat="server" ViewStateMode="Disabled" RenderOuterTable="false">
            <LayoutTemplate>
                <fieldset>
                    <legend>Log in Form</legend>
                    <ol>
                        <li>
                            <asp:Label ID="Label1" runat="server" AssociatedControlID="UserName">User name</asp:Label>
                            <asp:TextBox runat="server" ID="UserName" />
                        </li>
                        <li>
                            <asp:Label ID="Label2" runat="server" AssociatedControlID="Password">Password</asp:Label>
                            <asp:TextBox runat="server" ID="Password" TextMode="Password" />
                            
                        </li>
                        <li>
                            <asp:CheckBox runat="server" ID="RememberMe" />
                            <asp:Label ID="Label3" runat="server" AssociatedControlID="RememberMe" CssClass="checkbox">Remember me?</asp:Label>
                        </li>
                    </ol>
                    
                    <asp:Button ID="LoginButton" runat="server" CommandName="Login" Text="Log in" OnClick="Login_Button_Click" />
                </fieldset>
                <aside>
                    About Us
                    NWBA Bank  asdflkjhasdfjka
                    qerqwerqwer
                    qwerq
                    weertewtwe
                    fsadfasdf
                </aside>
            </LayoutTemplate>
        </asp:Login>
        <asp:Label ID="ErrorLabel" runat="server" Text="Check your username or password" Visible="False"></asp:Label>
    </section>

</asp:Content>
