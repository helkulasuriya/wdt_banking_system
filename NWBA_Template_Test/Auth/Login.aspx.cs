﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Security;
using NWBA_Template_Test.App_Code;

namespace NWBA_Template_Test.Auth
{
    public partial class Login : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

            if (Session.IsNewSession)
            {
                Login1.UserName = "";
                //Login1.Password = "";


            }
            else
            {
                //Ask to log in again--
                try
                {
                    if ((int)Session["Incorrect"] == 1)
                    {
                        ErrorLabel.Visible = true;

                    }
                }
                catch (NullReferenceException ejhfkjhg)
                {
                    
                    //throw;
                }
            }
        }

        protected void Login_Button_Click(object sender, EventArgs e)
        {
            Session["Incorrect"] = 0;
            UserFacade userDetails = new UserFacade();
            int check = userDetails.authenticateUser(Login1.UserName, Login1.Password);
            bool isAdmin = FormsAuthentication.Authenticate(Login1.UserName, Login1.Password);
            //string HashedPassword = FormsAuthentication.HashPasswordForStoringInConfigFile("admin", "sha1");
            if (check != -1)
            {
                Customer customer = userDetails.getUserById(check);
                Session["CustomerId"] = customer.CustomerID;
                Response.Redirect("~/User/ATM.aspx");
                Session.Timeout = 10;
                return;
            }

            else if(isAdmin)
            {
                Session["AdminID"] = Login1.UserName;
                //FormsAuthentication.RedirectFromLoginPage(Login1.UserName, true);
                Response.Redirect("~/AdminUser/Accounts.aspx");
                Session.Timeout = 10;
                return;
            }

            else
            {
                Session["Incorrect"] = 1;
                Response.Redirect("~/Auth/Login.aspx");
                ErrorLabel.Visible = true;
            }

       
        }
    }
}