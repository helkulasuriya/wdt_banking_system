﻿using System;

namespace NWBA_Template_Test.App_Code
{
    interface ITransactionFacade
    {
        string doDeposit(int accountNumber, decimal amount, string comments);
        string doWithDrawal(int accountNumber, decimal amount, string comments);
        string doTransfer(int sourceAccount, int destAccount, decimal amount, string comments);    
        System.Collections.Generic.List<NWBA_Template_Test.Account> getAccountsByUserandType(int customerID, string accountType);
        System.Collections.Generic.List<NWBA_Template_Test.Account> getAccountsByCustId(int customerID);
        decimal showBalance(int accNumber);
        System.Collections.Generic.List<NWBA_Template_Test.Transaction> showHistoryForAccount(int accountNumber);
        System.Collections.Generic.List<NWBA_Template_Test.Transaction> showHistoryForAccountForAdmin(int accountNumber);
        System.Collections.Generic.SortedList<String, Boolean> insertAccount(Decimal depositAmt, String accntType, Int32 customerId);
        System.Collections.Generic.List<Account> getAccountsByType(String accountType);
        bool doServiceChargeTransction(Transaction parentTransaction, decimal serviceChargeAmt);
    }
}
