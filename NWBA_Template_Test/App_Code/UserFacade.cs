﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace NWBA_Template_Test.App_Code
{
    public class UserFacade : NWBA_Template_Test.App_Code.IUserFacade
    {
        private UserDAO userDAO = new UserDAO();

        public int authenticateUser(string userId, string password)
        {
            //Boolean validity = false;
            Login login = new Login();
            login.UserID = userId;
            login.Password = password;
            //if (userDAO.checkLogin(login))
            //{
            //    validity = true;
            //}
            //return validity;
            return userDAO.checkLogin(login);
        }

        public Customer getUserById(Int32 customerId)
        {
            return userDAO.getUserById(customerId);
        }

        public String updateUser(Customer customer, string password)
        {
            return userDAO.updateUserDetails(customer, password);
        }

    }
}