﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Security.Cryptography;
using System.Text;

namespace NWBA_Template_Test.App_Code
{
    public class UserDAO
    {
        private bankDB_WDT1234Entities context = new bankDB_WDT1234Entities();

        public int checkLogin(Login login)
        {
            int validity = -1;
            try
            {
                Login user = context.Logins.Single(l => l.UserID == login.UserID);
                if (user != null)
                {
                    //Covert the login password into hashcode then check 
                    String encryptedPWD = _handleEncryption(login.Password);

                    if (user.Password.Equals(encryptedPWD))
                    {
                        validity = user.CustomerID;
                    }
                }

            }
            catch (Exception e)
            {
            }


            return validity;
        }

       
        private String _handleEncryption(String textPassword)
        {
            //Salting 
            textPassword += "SHA!?/";
            var provider = new SHA1CryptoServiceProvider();
            var encoding = new UnicodeEncoding();
            Byte[] hash = provider.ComputeHash(encoding.GetBytes(textPassword));
            string delimitedHexHash = BitConverter.ToString(hash);
            string hexHash = delimitedHexHash.Replace("-", "");
            //Hashed with a Salt
            return hexHash;
        }

        public Customer getUserById(int customerId)
        {
            Customer customer = new Customer();
            try
            {
                 customer = context.Customers.Single(c => c.CustomerID == customerId);
            }
            catch (Exception e)
            {
            }
            //Customer c = new Customer();
            return customer;
        }

        public String updateUserDetails(Customer customer, string password)
        {
            String message = String.Empty;
            Customer oldCustomer = new Customer();
            try
            {
                oldCustomer = context.Customers.Single(c => c.CustomerID == customer.CustomerID);
                if (oldCustomer != null)
                {
                    oldCustomer.CustomerName = customer.CustomerName;
                    oldCustomer.TFN = customer.TFN;
                    oldCustomer.Address = customer.Address;
                    oldCustomer.City = customer.City;
                    oldCustomer.State = customer.State;
                    oldCustomer.Phone = customer.Phone;
                    oldCustomer.PostCode = customer.PostCode;
Login changePassword = context.Logins.Single(l => l.CustomerID == customer.CustomerID);
                    if (changePassword != null && !password.Equals(string.Empty))
                    {
                        //encrypt the password
                        String encryptedPWD = _handleEncryption(password);

                        changePassword.Password = encryptedPWD;
                    }
                    message = "Successfully Updated";
                }
                else
                {
                    message = "User not found";
                }
                context.SaveChanges();

            }
            catch (Exception e)
            {
            }
            return message;
        }
    }
}