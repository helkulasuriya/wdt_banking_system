﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace NWBA_Template_Test.App_Code
{
    public class AdminBPay
    {
        public int BillPayID { get; set; }
        public int AccountNumber { get; set; }
        public string PayeeName { get; set; }
        public Nullable<decimal> Amount { get; set; }
        public System.DateTime ScheduleDate { get; set; }
        public string Period { get; set; }
        public System.DateTime ModifyDate { get; set; }
        public string RecurringStatus { get; set; }
    
    }
}