﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Diagnostics;

namespace NWBA_Template_Test.App_Code
{
    public class AccountDAO
    {
        private bankDB_WDT1234Entities context = new bankDB_WDT1234Entities();

        public Decimal getBalance(int accNumber)
        {
            var sourceAccTransctions = context.Transactions.Where(t => t.SourceAccount == accNumber);
            var destAccTransctions = context.Transactions.Where(t => t.DestinationAccount == accNumber);
            Decimal balance = 0;

            foreach (var t in sourceAccTransctions)
            {
                if (t.TransactionType == "D")
                    balance += Convert.ToDecimal(t.Amount);
                else
                    balance -= Convert.ToDecimal(t.Amount);
            }

            foreach (var t in destAccTransctions)
            {
                balance += Convert.ToDecimal(t.Amount);

            }
            //     int balance = dc.Transactions.Where(t=>t.a)
            return balance;
        }

        public Account getAccountById(Int32 accountNum)
        {
            Account retrievedAccnt = null;
            try
            {
                var accountsForId = from account in context.Accounts
                                    where account.AccountNumber.Equals(accountNum)
                                    select account;

                retrievedAccnt = (Account)accountsForId.ToList()[0];
            }
            catch (Exception ex)
            {
                Trace.WriteLine("Error in retrieving data : " + ex.StackTrace);
                return new Account();
            }
            return retrievedAccnt;
        }

        public List<Account> getAccountsByUserandType(Int32 customerID, String accountType)
        {
            List<Account> accountsList = null;
            try
            {
                var accountsForUser = from account in context.Accounts
                                      where account.CustomerID.Equals(customerID) && account.AccountType.Equals(accountType)
                                      select account;
                accountsList = accountsForUser.ToList<Account>();
            }
            catch (Exception ex)
            {
                Trace.WriteLine("Error in retrieving data : " + ex.StackTrace);
            }
            return accountsList;
        }

        public List<Account> getAccountsByCustId(Int32 customerID)
        {
            List<Account> accountsList = null;
            try
            {
                var accountsForUser = from account in context.Accounts
                                      where account.CustomerID.Equals(customerID)
                                      select account;
                accountsList = accountsForUser.ToList<Account>();
            }
            catch (Exception ex)
            {
                Trace.WriteLine("Error in retrieving data : " + ex.StackTrace);
            }
            return accountsList;
        }

        public Account insertAccountToDB(Account accountObj)
        {
            // Holds the newly created account object.
            Account newAccount = null;
            try
            {
                // Inserting an Account to the DB table.
                newAccount = context.Accounts.Add(new Account()
                {
                    AccountType = accountObj.AccountType,
                    CustomerID = accountObj.CustomerID,
                    ModifyDate = DateTime.Now
                });
                context.SaveChanges();
            }
            catch (Exception insertEx)
            {
                Trace.WriteLine("Error in inserting data : " + insertEx.StackTrace);
                return null;
            }
            return newAccount;
        }

        public List<Account> getAccountsByType(String accountType)
        {
            List<Account> accountsList = null;
            try
            {
                var accountsForUser = from account in context.Accounts
                                      where account.AccountType.Equals(accountType)
                                      select account;
                accountsList = accountsForUser.ToList<Account>();
            }
            catch (Exception ex)
            {
                Trace.WriteLine("Error in retrieving data : " + ex.StackTrace);
            }
            return accountsList;
        }
    }
}