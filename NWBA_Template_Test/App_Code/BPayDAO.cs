﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Web;
using System.Diagnostics;

namespace NWBA_Template_Test.App_Code
{
    public class BPayDAO
    {
        private bankDB_WDT1234Entities context = new bankDB_WDT1234Entities();

        public List<Payee> getAllPayeesList()
        {
            List<Payee> payeesList = null;
            try
            {
                var payeeRecords = from payee in context.Payees
                                   select payee;
                payeesList = payeeRecords.ToList<Payee>();
            }
            catch (Exception ex)
            {
                Trace.WriteLine("Error in retrieving data : " + ex.StackTrace);
            }
            return payeesList;
        }

        public List<BillPay> getAllBillPaysList(int customerID)
        {
            List<BillPay> billPaysList = null;
            try
            {
                var billPayRecords = from billPay in context.BillPays
                                     where billPay.Account.CustomerID == customerID
                                     select billPay;
                billPaysList = billPayRecords.ToList<BillPay>();
            }
            catch (Exception ex)
            {
                Trace.WriteLine("Error in retrieving data : " + ex.StackTrace);
            }
            return billPaysList;
        }

        public List<BillPay> getActiveBillPaysList()
        {
            List<BillPay> billPaysList = null;
            try
            {
                var billPayRecords = from billPay in context.BillPays
                                     where billPay.RecurringStatus == "active" 
                                     select billPay;
                billPaysList = billPayRecords.ToList<BillPay>();
            }
            catch (Exception ex)
            {
                Trace.WriteLine("Error in retrieving data : " + ex.StackTrace);
            }
            return billPaysList;
        }

        public List<AdminBPay> getBillPaysWithPayeeName(int customerID)
        {
            List<AdminBPay> billPaysList = null;

            try
            {
                var billPayRecords = from billPay in context.BillPays 
                                     where billPay.Account.CustomerID == customerID

                                     select new AdminBPay
                                     {
                                         BillPayID = billPay.BillPayID,

                                         PayeeName = billPay.Payee.PayeeName,
                                         Amount = billPay.Amount,

                                         ScheduleDate = billPay.ScheduleDate,
                                         Period = billPay.Period,
                                         RecurringStatus = billPay.RecurringStatus,
                                         AccountNumber = billPay.AccountNumber
                                     };

                billPaysList = billPayRecords.ToList<AdminBPay>();
            }
            catch (Exception ex)
            {
                Trace.WriteLine("Error in retrieving data : " + ex.StackTrace);
            }
            return billPaysList;
        }

        public Customer getCustomerByAccountID(int accountID)
        {
            Customer customer = null;
            try
            {
                customer = (Customer)(from account in context.Accounts
                                      where account.AccountNumber == accountID
                                      select account.Customer);
            }
            catch (Exception ex)
            {
                Trace.WriteLine("Error in retrieving data : " + ex.StackTrace);
            }
            return customer;
        }

        public BillPay insertBillPaytoDB(BillPay billPayObj)
        {
            // Holds the newly created inserted Bill Pay object.
            BillPay newBillPay = null;
            try
            {
                // Inserting an Bill Pay record to the DB table.
                newBillPay = context.BillPays.Add(new BillPay()
                {
                    AccountNumber = billPayObj.AccountNumber,
                    PayeeID = billPayObj.PayeeID,
                    Amount = billPayObj.Amount,
                    ScheduleDate = billPayObj.ScheduleDate,
                    Period = billPayObj.Period,
                    RecurringStatus = billPayObj.RecurringStatus,
                    ModifyDate = DateTime.Now
                });
                context.SaveChanges();
            }
            catch (Exception insertEx)
            {
                Trace.WriteLine("Error in inserting data : " + insertEx.StackTrace);
                return null;
            }
            return newBillPay;
        }

        public String modifyBillPay(BillPay updatedBillPay)
        {
            try
            {
                // Selecting the record to that needs to be updated.
                BillPay existingBPayObj = (from billPay in context.BillPays
                                           where billPay.BillPayID == updatedBillPay.BillPayID
                                           select billPay).First();
                // Updates the fields with new values.
                existingBPayObj.AccountNumber = updatedBillPay.AccountNumber;
                existingBPayObj.PayeeID = updatedBillPay.PayeeID;
                existingBPayObj.Amount = updatedBillPay.Amount;
                existingBPayObj.ScheduleDate = updatedBillPay.ScheduleDate;
                existingBPayObj.RecurringStatus = updatedBillPay.RecurringStatus;
                existingBPayObj.Period = updatedBillPay.Period;
                existingBPayObj.ModifyDate = DateTime.Now;
                context.SaveChanges();
            }
            catch (Exception insertEx)
            {
                Trace.WriteLine("Error in modifying data : " + insertEx.StackTrace);
                return null;
            }
            return "Successfully modified record.";
        }

        public String updateScheduledDateByID(int oldBPayID)
        {
            try
            {
                // Selecting the record to that needs to be updated.
                BillPay existingBPayObj = (from billPay in context.BillPays
                                           where billPay.BillPayID == oldBPayID
                                           select billPay).First();
                DateTime oldScheduledDate = existingBPayObj.ScheduleDate;

                // Calculate and updates the next schedule time.
                while (!(existingBPayObj.ScheduleDate > DateTime.Now))
                {
                    switch (existingBPayObj.Period)
                    {
                        case "d":
                            existingBPayObj.ScheduleDate = oldScheduledDate.AddDays(1);
                            break;
                        case "w":
                            existingBPayObj.ScheduleDate = oldScheduledDate.AddDays(7);
                            break;
                        case "m":
                            existingBPayObj.ScheduleDate = oldScheduledDate.AddMonths(1);
                            break;
                        case "q":
                            existingBPayObj.ScheduleDate = oldScheduledDate.AddMonths(3);
                            break;
                        case "a":
                            existingBPayObj.ScheduleDate = oldScheduledDate.AddYears(1);
                            break;
                        case "o":
                            existingBPayObj.ScheduleDate = oldScheduledDate;
                            break;
                    }
                }
                existingBPayObj.ModifyDate = DateTime.Now;
                context.SaveChanges();
            }
            catch (Exception insertEx)
            {
                Trace.WriteLine("Error in inserting data : " + insertEx.StackTrace);
                return null;
            }
            return "Success in changing the date";
        }

        public String updateBPayStatusByID(int oldBPayID, String newStatus)
        {
            try
            {
                // Selecting the record to that needs to be updated.
                BillPay existingBPayObj = (from billPay in context.BillPays
                                           where billPay.BillPayID == oldBPayID
                                           select billPay).First();
                // Updates the new status to be active or inactive.
                existingBPayObj.RecurringStatus = newStatus;
                existingBPayObj.ModifyDate = DateTime.Now;
                context.SaveChanges();
            }
            catch (Exception insertEx)
            {
                Trace.WriteLine("Error in inserting data : " + insertEx.StackTrace);
                return null;
            }
            return "Success in changing the status";
        }
    }
}