﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Diagnostics;
using System.Collections;

namespace NWBA_Template_Test.App_Code
{
    public class TransactionDAO
    {
        private bankDB_WDT1234Entities context = new bankDB_WDT1234Entities();

        public List<Transaction> getTransactionsByAcntNum(Int32 accountNumber)
        {
            List<Transaction> transactionList = null;
            try
            {
                var retrievedtransacs = from transaction in context.Transactions
                                        where transaction.SourceAccount.Equals(accountNumber)
                                        select transaction;
                                        

                transactionList = retrievedtransacs.ToList();

                var retrievedTransfers = from transaction in context.Transactions
                                         where transaction.DestinationAccount.Equals(accountNumber)
                                         select transaction;
                transactionList.AddRange(retrievedTransfers.ToList()); 
            }
            catch (Exception ex)
            {
                Trace.WriteLine("Error in retrieving data : " + ex.StackTrace);
            }
            return transactionList;
        }

        public bool hasFreeTransaction(int accountId)
        {
            int TranstCount = context.Transactions.Where(t => t.SourceAccount == accountId).Count();
            
            System.Diagnostics.Trace.WriteLine("num of accounts... " + TranstCount);
            if (TranstCount < 4)
            {
                return true;
            }

            return false;
        }

        public bool addTranscInDB(Transaction transact)
        {
            try
            {
                context.Transactions.Add(new Transaction()
                {
                    TransactionType = transact.TransactionType,
                    SourceAccount = transact.SourceAccount,
                    DestinationAccount = transact.DestinationAccount,
                    Amount = transact.Amount,
                    Comment = transact.Comment,
                    ModifyDate = DateTime.Now
                });
                context.SaveChanges();

                return true;
            }
            catch (Exception e) //yet to find specific exception type
            {
                return false;
            }
        }
                
    }
}