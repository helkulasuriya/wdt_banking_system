﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace NWBA_Template_Test.App_Code
{
    public class TransactionFacade : NWBA_Template_Test.App_Code.ITransactionFacade
    {
        private TransactionDAO transactionDAO = new TransactionDAO();
        private AccountDAO accountDAO = new AccountDAO();

        public List<Account> getAccountsByUserandType(Int32 customerID, String accountType)
        {
            return accountDAO.getAccountsByUserandType(customerID, accountType);
        }

        public decimal showBalance(int accNumber)
        {
            return accountDAO.getBalance(accNumber);
        }

        public System.Collections.Generic.List<NWBA_Template_Test.Account> getAccountsByCustId(int customerID)
        {
            return accountDAO.getAccountsByCustId(customerID);
        }

        public String doWithDrawal(int accountNumber, Decimal amount, string comments)
        {
            // Get the account object.
            Account account = accountDAO.getAccountById(accountNumber);

            //Create the transaction object to carry on the transaction
            Transaction withdrawal = new Transaction();
            withdrawal.TransactionType = "W";
            withdrawal.SourceAccount = accountNumber;
            withdrawal.Amount = amount;
            withdrawal.Comment = comments;
            if (account != null)
                withdrawal.Account = account;
            else
                withdrawal.Account = new Account();

            // Make withdraw transaction.
            if (doATMTransction(withdrawal) == null)
            {
                return "Done";
            }

            return "Error in withdraw transaction. \n Please insert correct values and try again.";
        }

        public String doDeposit(int accountNumber, decimal amount, string comments)
        {
            // Get the account object.
            Account account = accountDAO.getAccountById(accountNumber);

            //Create the transaction object
            Transaction deposit = new Transaction();
            deposit.TransactionType = "D";
            deposit.SourceAccount = accountNumber;
            deposit.Amount = amount;
            deposit.Comment = comments;
            if (account != null)
                deposit.Account = account;
            else
                deposit.Account = new Account();

            // Make deposit transaction.
            if (doATMTransction(deposit) == null)
            {
                return "Done";
            }
            return "Error in inserting deposit transaction. \n Please insert correct values and try again.";
        }

        public String doTransfer(int sourceAccount, int destAccount, decimal amount, string comments)
        {
            Transaction fromAccountTransfer = new Transaction();

            // Get the account object.
            Account fromAccount = accountDAO.getAccountById(sourceAccount);
            //Check if null
            if (fromAccount == null)
            {
                return "Transfer Error at this point";
            }

            //Check if enough balance
            fromAccountTransfer.Amount = amount;
            fromAccountTransfer.Comment = comments;
            fromAccountTransfer.DestinationAccount = destAccount;
            fromAccountTransfer.SourceAccount = sourceAccount;
            fromAccountTransfer.TransactionType = "T";
            fromAccountTransfer.Account = fromAccount;

            if (doATMTransction(fromAccountTransfer) == null)
            {
                return "Successfully transfered";
            }

            return "Transaction failed !!! \n couldn't add transaction to Database";
        }

        private String doATMTransction(Transaction transact)
        {
            if (transact.Account != null)
            {
                bool insertStatus = false;

                Decimal hasFreeTransacCharge = Convert.ToDecimal(transactionDAO.hasFreeTransaction(transact.SourceAccount) ? 0 : 0.20);
                Decimal total = 0.0M;
                if (transact.TransactionType.Equals("W") || transact.TransactionType.Equals("T"))
                    total = accountDAO.getBalance(transact.SourceAccount) - Convert.ToDecimal(transact.Amount) - hasFreeTransacCharge;
                else
                    total = accountDAO.getBalance(transact.SourceAccount) + Convert.ToDecimal(transact.Amount) - hasFreeTransacCharge;

                System.Diagnostics.Trace.WriteLine("The calculated total is: " + total);

                if (transact.Account.AccountType.Equals("C"))
                {
                    if (!(total > 200))
                        return "not enough balance to do tranction";
                }
                if (transact.Account.AccountType.Equals("S"))
                {
                    if (!(total > 0))
                        return "Balance Error : Not enough balance to do transaction";
                }

                //if (!(transact.Amount == 0))
                //{
                    insertStatus = transactionDAO.addTranscInDB(transact);
                //}
                if (!(transact.Amount == 0) && (insertStatus == false))
                    return "Transaction failed !!! \n couldn't add transaction to Database";
                else
                {
                    if (!hasFreeTransacCharge.Equals(0))
                    {
                        doServiceChargeTransction(transact, 0.20M);
                    }
                    return null;
                }
            }
            else
            {   //Error
                return "Transaction failed !!! \n couldn't add transaction to Database";
            }
        }

        public bool doServiceChargeTransction(Transaction parentTransaction, decimal serviceChargeAmt)
        {
            Transaction serviceTrasac = new Transaction();
            serviceTrasac.TransactionType = "S";
            serviceTrasac.Amount = serviceChargeAmt;
            serviceTrasac.SourceAccount = parentTransaction.SourceAccount;
            serviceTrasac.Comment = "Applying service charge";

            if (transactionDAO.addTranscInDB(serviceTrasac))
                return true;

            return false;
        }

        public List<Transaction> showHistoryForAccount(Int32 accountNumber)
        {
            // Get the account object.
            Account account = accountDAO.getAccountById(accountNumber);

            //Create the parent transaction object
            Transaction serviceCharge = new Transaction();
            serviceCharge.TransactionType = "S";
            serviceCharge.SourceAccount = accountNumber;
            serviceCharge.Amount = 0M;
            serviceCharge.Comment = "View statement service charge";
            if (account != null)
                serviceCharge.Account = account;
            else
                serviceCharge.Account = new Account();

            List<Transaction> transactionList = null;

            if (doATMTransction(serviceCharge) == null)
            {
                // Retrieving the transaction list.
                transactionList = transactionDAO.getTransactionsByAcntNum(accountNumber);
                return transactionList;
            }

            return null;
        }


        public List<Transaction> showHistoryForAccountForAdmin(Int32 accountNumber)
        {
            // Get the account object.
            Account account = accountDAO.getAccountById(accountNumber);

            List<Transaction> transactionList = null;

                // Retrieving the transaction list.
                transactionList = transactionDAO.getTransactionsByAcntNum(accountNumber);
                return transactionList;
            

       
        }


        public SortedList<String, Boolean> insertAccount(Decimal depositAmt, String accntType, Int32 customerId)
        {
            // Map to contain the insert status and error or info message.
            SortedList<String, Boolean> statusMap = new SortedList<String, Boolean>();

            if ((accntType.Equals("S") && (depositAmt < 100)) || (accntType.Equals("C") && (depositAmt < 500)))
            {
                statusMap.Add("Not enough balance to open an account. \n Please deposit a higher amount.\n", false);
                return statusMap;
            }
            else
            {
                Account newAccount = new Account();
                newAccount.CustomerID = customerId;
                newAccount.AccountType = accntType;
                // Calling account insert function.
                Account addedAccountObj = accountDAO.insertAccountToDB(newAccount);

                if (addedAccountObj == null)
                {
                    statusMap.Add("Error in inserting account. \n", false);
                    return statusMap;
                }

                // Inserting a trasaction for creation money deposit.
                Transaction accCreationTransac = new Transaction();
                accCreationTransac.TransactionType = "D";
                accCreationTransac.SourceAccount = addedAccountObj.AccountNumber;
                accCreationTransac.Account = addedAccountObj;
                accCreationTransac.Amount = depositAmt;
                accCreationTransac.Comment = "Account creation deposit";

                if (doATMTransction(accCreationTransac) == null)
                    statusMap.Add("Successfully inserted account. \n", true);
                else
                    statusMap.Add("Failed to insert creation deposit. \n", false);
            }
            return statusMap;
        }

        public List<Account> getAccountsByType(String accountType)
        {
            return accountDAO.getAccountsByType(accountType);
        }
    }
}