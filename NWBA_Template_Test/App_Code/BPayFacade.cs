﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Threading;

namespace NWBA_Template_Test.App_Code
{
    public class BPayFacade : NWBA_Template_Test.App_Code.IBPayFacade
    {
        private BPayDAO bPayDAO = new BPayDAO();
        private ITransactionFacade trasactFacade = new TransactionFacade();
        private TransactionDAO transactionDAO = new TransactionDAO();
        private AccountDAO accountDAO = new AccountDAO();

        public static SortedList<String, Object[]> newBillPaymentsList = new SortedList<String, Object[]>();

        public List<Payee> getAllPayeesList()
        {
            return bPayDAO.getAllPayeesList();
        }

        public List<BillPay> getAllBillPaysList(int customerID)
        {
            return bPayDAO.getAllBillPaysList(customerID);
        }

        public BillPay addBillPayRecord(BillPay billPayObj)
        {
            return bPayDAO.insertBillPaytoDB(billPayObj);
        }

        public String modifyBillPayRecord(BillPay updatedBillPay)
        {
            return bPayDAO.modifyBillPay(updatedBillPay);
        }

		public List<AdminBPay> getBillPaysWithPayeeName(int customerID)
        {
            return bPayDAO.getBillPaysWithPayeeName(customerID);
        }
		
        public void scheduleRecurringPayment()
        {
            while (true)
            {
                List<BillPay> activeBillPayList = bPayDAO.getActiveBillPaysList();

                if ((activeBillPayList != null) && !(activeBillPayList.Count == 0))
                {
                    foreach (BillPay currentBillPay in activeBillPayList)
                    {
                        if (DateTime.Now >= currentBillPay.ScheduleDate)
                        {
                            // Obtaining the associated account object.
                            Account account = accountDAO.getAccountById(currentBillPay.AccountNumber);

                            Transaction transaction = new Transaction();
                            transaction.TransactionType = "B";
                            transaction.SourceAccount = currentBillPay.AccountNumber;
                            transaction.DestinationAccount = null;
                            transaction.Amount = currentBillPay.Amount;
                            transaction.Comment = "Bill Pay for Payee " + currentBillPay.PayeeID;
                            transaction.ModifyDate = DateTime.Now;
                            if (account != null)
                                transaction.Account = account;

                            if (doBillPayTrasaction(transaction) != null)
                            {
                                bPayDAO.updateBPayStatusByID(currentBillPay.BillPayID, "failed");
                            }
                            else if (currentBillPay.Period == "o")
                            {
                                bPayDAO.updateBPayStatusByID(currentBillPay.BillPayID, "done");
                            }
                            else
                                bPayDAO.updateScheduledDateByID(currentBillPay.BillPayID);
                        }
                    }
                }
            }
        }

        private String doBillPayTrasaction(Transaction transact)
        {
            if (transact.Account != null)
            {
                bool insertStatus = false;

                Decimal hasFreeTransacCharge = Convert.ToDecimal(transactionDAO.hasFreeTransaction(transact.SourceAccount) ? 0 : 0.20);
                Decimal total = 0.0M;
                if (transact.TransactionType.Equals("W") || transact.TransactionType.Equals("T"))
                    total = accountDAO.getBalance(transact.SourceAccount) - Convert.ToDecimal(transact.Amount) - hasFreeTransacCharge;
                else
                    total = accountDAO.getBalance(transact.SourceAccount) + Convert.ToDecimal(transact.Amount) - hasFreeTransacCharge;

                System.Diagnostics.Trace.WriteLine("The calculated total is: " + total);

                if (transact.Account.AccountType.Equals("C"))
                {
                    if (!(total > 200))
                        return "not enough balance to do tranction";
                }
                if (transact.Account.AccountType.Equals("S"))
                {
                    if (!(total > 0))
                        return "Balance Error : Not enough balance to do transaction";
                }

                //if (!(transact.Amount == 0))
                //{
                insertStatus = transactionDAO.addTranscInDB(transact);
                //}
                if (!(transact.Amount == 0) && (insertStatus == false))
                    return "Transaction failed !!! \n couldn't add transaction to Database";
                else
                {
                    if (!hasFreeTransacCharge.Equals(0))
                    {
                        trasactFacade.doServiceChargeTransction(transact, 0.30M);
                    }
                    return null;
                }
            }
            else
            {   //Error
                return "Transaction failed !!! \n couldn't add transaction to Database";
            }
        }

		public String updateBPayStatusByID(int oldBPayID, string newStatus)
        {
            return bPayDAO.updateBPayStatusByID(oldBPayID, newStatus);
        }
    }
}