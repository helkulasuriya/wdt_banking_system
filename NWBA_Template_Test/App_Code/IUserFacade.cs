﻿using System;
namespace NWBA_Template_Test.App_Code
{
    interface IUserFacade
    {
        int authenticateUser(string userId, string password);
        NWBA_Template_Test.Customer getUserById(Int32 customerId);
        string updateUser(NWBA_Template_Test.Customer customer, string password);
    }
}
