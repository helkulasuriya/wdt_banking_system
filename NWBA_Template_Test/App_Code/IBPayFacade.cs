﻿using System;
using System.Collections.Generic;

namespace NWBA_Template_Test.App_Code
{
    interface IBPayFacade
    {
        NWBA_Template_Test.BillPay addBillPayRecord(NWBA_Template_Test.BillPay billPayObj);
        List<NWBA_Template_Test.Payee> getAllPayeesList();
        void scheduleRecurringPayment();
        List<BillPay> getAllBillPaysList(int customerID);
        String modifyBillPayRecord(BillPay updatedBillPay);
		String updateBPayStatusByID(int oldBPayID,string newStatus);
        List<AdminBPay> getBillPaysWithPayeeName(int customerID);


    }
}
