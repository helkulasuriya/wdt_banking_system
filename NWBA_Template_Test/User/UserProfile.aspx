﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="UserProfile.aspx.cs" Inherits="NWBA_Template_Test.User.UserProfile" MasterPageFile="User.Master" %>

<asp:Content runat="server" ID="BodyContent" ContentPlaceHolderID="MainContent">

    
        <table id="UserDetailsTable" style="border-style:None;">
	<caption>
		UserDetails
	</caption><tr align="left">
		<td align="left">Name</td><td>
                <asp:TextBox ID="Name" runat="server"></asp:TextBox>
                </td>
	</tr><tr>
		<td align="left">TFN</td><td>
                <asp:TextBox ID="TFN" runat="server"></asp:TextBox>
                </td>
	</tr><tr>
		<td align="left">Address</td><td>
                <asp:TextBox ID="Address" runat="server" Height="16px"></asp:TextBox>
                </td>
	</tr><tr>
		<td align="left">City</td><td>
                <asp:TextBox ID="City" runat="server"></asp:TextBox>
                </td>
	</tr><tr>
		<td align="left">State</td><td>
                <asp:TextBox ID="State" runat="server"></asp:TextBox>
                </td><td>
                    <asp:RegularExpressionValidator ID="StateLengthValidator" runat="server" ControlToValidate="State" ErrorMessage="State must be 2 or 3 lettered Australian State only" ValidationExpression="^([Vv][Ii][Cc])|([Nn][Ss][Ww])|([Qq][Ll][Dd])|([Nn][Tt])|([Ss][Aa])|([Ww][Aa])|([Aa][Cc][Tt])|([Tt][Aa][Ss])$"></asp:RegularExpressionValidator>
                     </td>
	</tr><tr>
		<td align="left">Postcode</td><td>
                <asp:TextBox ID="Postcode" runat="server" MaxLength="4"></asp:TextBox>
                </td><td>
                    <asp:RangeValidator ID="PostCodeRange" runat="server" ControlToValidate="Postcode" ErrorMessage="PostCode must be of 4-digits" MaximumValue="9999" MinimumValue="1000" Type="Integer"></asp:RangeValidator>
                     </td>
	</tr><tr>
		<td align="left">Phone</td><td>
                <asp:TextBox ID="Phone" runat="server"></asp:TextBox>
                </td><td>
        <%--<asp:RegularExpressionValidator ID="PhoneValidator" runat="server" ErrorMessage="The phone number should be of (61) - XXXX XXXX format" ControlToValidate="Phone" SetFocusOnError="True" ValidateRequestMode="Enabled" ValidationExpression="\(61\)-\d{4} \d{4}"></asp:RegularExpressionValidator>--%>

                     </td>
	</tr><tr>
		<td align="left">Password</td><td>
                <asp:TextBox ID="Password" runat="server" TextMode="Password"></asp:TextBox>
                </td>
	</tr><tr>
		<td align="left">Confirm Password</td><td>
                <asp:TextBox ID="ConfirmPassword" runat="server" TextMode="Password"></asp:TextBox>
                </td><td>
                    <asp:CompareValidator ID="ConfirmPasswordValidator" runat="server" ErrorMessage="The passwords entered don't match" ControlToCompare="Password" ControlToValidate="ConfirmPassword" Font-Italic="True"></asp:CompareValidator>
                     </td>
	</tr><tr>
		<td>
            <asp:Button ID="Clear" runat="server" OnClick="Clear_Button_Click" Text="Clear" />
                </td><td>
                    <asp:Button ID="Update" runat="server" OnClick="Update_Button_Click" Text="Update" />
                </td>
	</tr>
</table>
        
        <asp:Label ID="Updation" runat="server" Text="Label" Visible="False"></asp:Label>
        <%--<asp:CompareValidator ID="StateValidator" runat="server" ControlToValidate="State" ErrorMessage="The State must be a 3-lettered Australian State" Operator="DataTypeCheck" Type="String"></asp:CompareValidator>--%>
        <asp:Label ID="ErrorLabel" runat="server" Text="Some Error Occurred" Visible="False"></asp:Label>
    


         
    


</asp:Content>
