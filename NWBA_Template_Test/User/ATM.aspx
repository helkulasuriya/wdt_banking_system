﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ATM.aspx.cs" Inherits="NWBA_Template_Test.User.ATM" MasterPageFile="User.Master" %>

<asp:Content runat="server" ID="BodyContent" ContentPlaceHolderID="MainContent">

    <div id="content">
        <!-- <img id="top" src="/Images/top.png" alt="" /> -->
        <asp:Panel ID="Panel1" runat="server">
            <fieldset>
                <legend>Select Account for Transaction</legend>
                <div style="float: left;">
                    <ol>
                        <li>
                            <asp:Label ID="errorLabel" runat="server" Font-Italic="True" ForeColor="Red"></asp:Label>
                        </li>
                        <li>
                            <asp:Label ID="Label7" runat="server" AssociatedControlID="fromAccountDropBox">From Account</asp:Label>
                            <asp:DropDownList ID="fromAccountDropBox" runat="server" AutoPostBack="True" OnSelectedIndexChanged="Change_To_Accounts">
                            </asp:DropDownList>
                        </li>
                    </ol>
                </div>
            </fieldset>

            <fieldset>
                <legend>Deposit</legend>
                <div style="float: left;">
                    <ol>
                        <li>
                            <asp:Label ID="depositErrorLabel" runat="server" Font-Italic="True" ForeColor="Red"></asp:Label>
                        </li>                        
                        <li>
                            <asp:Label ID="Label1" runat="server" AssociatedControlID="depositAmont">Deposit Amount</asp:Label>
                            <asp:TextBox ID="depositAmont" runat="server" Width="130px" MaxLength="11" />
                            <%--<asp:RequiredFieldValidator ID="depositRequiredValidator" runat="server" ErrorMessage="Deposit amount is required. Please Insert." ForeColor="#CC0000" ControlToValidate="depositAmont" Display="Dynamic"></asp:RequiredFieldValidator>--%>
                            <asp:RegularExpressionValidator ID="depositExpressionValidator" runat="server" ErrorMessage="Please insert a correct currency value." Display="Dynamic" EnableClientScript="True" ControlToValidate="depositAmont" ForeColor="Red" ValidationExpression="[0-9]+(.[0-9]{1,2})?$"></asp:RegularExpressionValidator>
                        </li>
                        <li>                            
                            <asp:Label ID="Label3" runat="server" Text="Comments"></asp:Label>
                            <asp:TextBox ID="DepositComments" runat="server" TextMode="MultiLine"></asp:TextBox>                            
                        </li>
                    </ol>

                    <asp:Button ID="DepositButton" runat="server" Text="Deposit" CommandName="Deposit" OnClick="DepositButton_Click" UseSubmitBehavior="False" />
                </div>
            </fieldset>

            <fieldset>
                <legend>Withdraw</legend>
                <div style="float: left;">
                    <ol>
                        <li>
                            <asp:Label ID="withdrawErrorLabel" runat="server" Font-Italic="True" ForeColor="Red"></asp:Label>
                        </li>
                        <li>
                            <asp:Label ID="Label2" runat="server" AssociatedControlID="withdrawAmount">Withdraw Amount</asp:Label>
                            <asp:TextBox runat="server" ID="withdrawAmount" MaxLength="11" />
                            <%--<asp:RequiredFieldValidator ID="withdrawRequiredValidator" runat="server" ErrorMessage="Deposit amount is required. Please Insert." ForeColor="#CC0000" ControlToValidate="withdrawAmount"></asp:RequiredFieldValidator>--%>
                            <asp:RegularExpressionValidator ID="withdrawExpressionValidator" runat="server" ErrorMessage="Please insert a correct currency value." EnableClientScript="True" ControlToValidate="withdrawAmount" ForeColor="Red" ValidationExpression="[0-9]+(.[0-9]{1,2})?$" Display="Dynamic"></asp:RegularExpressionValidator>
                        </li>
                        <li>                            
                            <asp:Label ID="Label8" runat="server" Text="Comments"></asp:Label>
                            <asp:TextBox ID="WithdrawComment" runat="server" TextMode="MultiLine"></asp:TextBox>
                            
                        </li>
                    </ol>

                    <asp:Button ID="WithdrawButton" runat="server" Text="Withdraw" CommandName="Withdraw" OnClick="WithdrawButton_Click" />
                </div>
            </fieldset>

            <fieldset>
                <legend>Transfer Money</legend>
                <div style="float: left;">
                    <ol>                        
                        <li>
                            <asp:Label ID="transferErrorLabel" runat="server" Font-Italic="True" ForeColor="Red"></asp:Label>
                        </li>
                        <li>
                            <asp:Label ID="Label4" runat="server" AssociatedControlID="toAccount">To Account</asp:Label>
                            <asp:DropDownList ID="toAccount" runat="server"></asp:DropDownList>
                        </li>
                        <li>
                            <asp:Label ID="Label5" runat="server" AssociatedControlID="transferAmount">Amount</asp:Label>
                            <asp:TextBox runat="server" ID="transferAmount" />
                            <asp:RegularExpressionValidator ID="transferExpressionValidator" runat="server" ErrorMessage="Please insert a correct currency value." ControlToValidate="transferAmount" ForeColor="Red" ValidationExpression="[0-9]+(.[0-9]{1,2})?$" Display="Dynamic"></asp:RegularExpressionValidator>
                        </li>
                        <li>
                            <asp:Label ID="Label6" runat="server" AssociatedControlID="TransferComment">Comment</asp:Label>
                            <asp:TextBox runat="server" ID="TransferComment" TextMode="MultiLine" />
                        </li>
                    </ol>
                    
                    <asp:Button ID="TransferButton" runat="server" Text="Transfer" OnClick="TransferButton_Click" />
                </div>
            </fieldset>
        </asp:Panel>
    </div>
</asp:Content>
