﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Diagnostics;

using NWBA_Template_Test.App_Code;

namespace NWBA_Template_Test.User
{
    public partial class ATM : System.Web.UI.Page
    {
        private ITransactionFacade transactionFacade = new TransactionFacade();

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session.IsNewSession || ((Session.Keys == null) || (Session.Keys.Count == 0)))
                Response.Redirect("/Auth/Login.aspx");

            if (!IsPostBack)
                populateAccountDropBox();
        }

        private void populateAccountDropBox()
        {
            Int32 customerId = (Int32)Session["CustomerId"];
            //Int32 customerId = 1;
            List<Account> savingsAccounts = transactionFacade.getAccountsByUserandType(customerId, "S");
            List<Account> checkingAccounts = transactionFacade.getAccountsByUserandType(customerId, "C");
            int counter = 1;

            fromAccountDropBox.Items.Clear();
            fromAccountDropBox.Items.Add("None..");
            fromAccountDropBox.SelectedIndex = 0;

            if ((savingsAccounts != null) && !(savingsAccounts.Count() == 0))
            {
                foreach (Account accnt in savingsAccounts)
                {
                    String item = "Savings";
                    if (accnt != null)
                    {
                        item += " - " + accnt.AccountNumber + " -   AUS$ " + transactionFacade.showBalance(accnt.AccountNumber);
                    }
                    fromAccountDropBox.Items.Insert(counter++, new ListItem(item, (accnt.AccountNumber).ToString()));

                }
            }
            if ((checkingAccounts != null) && !(checkingAccounts.Count() == 0))
            {
                foreach (Account accnt in checkingAccounts)
                {
                    String item = "Checking";
                    if (accnt != null)
                    {
                        item += " - " + accnt.AccountNumber + " -   AUS$ " + transactionFacade.showBalance(accnt.AccountNumber);

                    }
                    fromAccountDropBox.Items.Insert(counter++, new ListItem(item, (accnt.AccountNumber).ToString()));
                }
            }
        }

        protected void DepositButton_Click(object sender, EventArgs e)
        {
            Int32 accountNum = getSelectedAccount();
            if (accountNum == 0)
                return;

            if (depositAmont.Text.Equals(""))
            {
                depositErrorLabel.Text = "Deposit amount is required. Please insert a value.";
                return;
            }
            TransactionFacade transact = new TransactionFacade();
            depositErrorLabel.Text = transact.doDeposit(accountNum, Convert.ToDecimal(depositAmont.Text), DepositComments.Text);
        }

        protected void WithdrawButton_Click(object sender, EventArgs e)
        {
            Int32 accountNum = getSelectedAccount();
            if (accountNum == 0)
                return;

            if (withdrawAmount.Text.Equals(""))
            {
                withdrawErrorLabel.Text = "Withdraw amount is required. Please insert a value.";
                return;
            }
            TransactionFacade withdraw = new TransactionFacade();
            withdrawErrorLabel.Text = withdraw.doWithDrawal(accountNum, Convert.ToDecimal(withdrawAmount.Text), WithdrawComment.Text);
        }

        protected void TransferButton_Click(object sender, EventArgs e)
        {
            Int32 fromAccountNum = getSelectedAccount();
            Int32 toAccountNum = getSelectedToAccount();

            if ((fromAccountNum == 0) || (toAccountNum == 0))
                return;

            if (transferAmount.Text.Equals(""))
            {
                transferErrorLabel.Text = "Transfer amount is required. Please insert a value.";
                return;
            }
            TransactionFacade transfer = new TransactionFacade();
            transferErrorLabel.Text = transfer.doTransfer(fromAccountNum, toAccountNum, Convert.ToDecimal(transferAmount.Text), TransferComment.Text);
        }

        protected void Change_To_Accounts(object sender, EventArgs e)
        {
            List<Account> savingsAccounts = transactionFacade.getAccountsByType("S");
            List<Account> checkingAccounts = transactionFacade.getAccountsByType("C");
            int counter = 1;

            toAccount.Items.Clear();
            toAccount.Items.Add("None..");
            toAccount.SelectedIndex = 0;

            Int32 selectedFromAccount = -1;
            try
            {
                selectedFromAccount = Int32.Parse(fromAccountDropBox.SelectedValue);
            }
            catch (Exception exc)
            {   //handler
            }
            if ((savingsAccounts != null) && !(savingsAccounts.Count() == 0))
            {
                foreach (Account accnt in savingsAccounts)
                {
                    if (accnt.AccountNumber != selectedFromAccount)
                    {
                        String toItems = "Savings";
                        if (accnt != null)
                            toItems += " - " + accnt.AccountNumber + " -   AUS$ " + transactionFacade.showBalance(accnt.AccountNumber);

                        toAccount.Items.Insert(counter++, new ListItem(toItems, (accnt.AccountNumber).ToString()));
                    }
                }
            }
            if ((checkingAccounts != null) && !(checkingAccounts.Count() == 0))
            {
                foreach (Account accnt in checkingAccounts)
                {
                    if (accnt.AccountNumber != selectedFromAccount)
                    {
                        String toItems = "Checkings";
                        if (accnt != null)
                            toItems += " - " + accnt.AccountNumber + " -   AUS$ " + transactionFacade.showBalance(accnt.AccountNumber);

                        toAccount.Items.Insert(counter++, new ListItem(toItems, (accnt.AccountNumber).ToString()));
                    }
                }
            }
        }

        private Int32 getSelectedAccount()
        {
            string selectedAccntNum = fromAccountDropBox.SelectedValue;
            Int32 accountNum = 0;
            try
            {
                accountNum = Int32.Parse(selectedAccntNum);
                if (selectedAccntNum.Equals(0))
                {   //error
                    errorLabel.Text = "\nPlease select a correct account to proceed.\n";
                    return 0;
                }
            }
            catch (FormatException parseEx)
            {
                errorLabel.Text = "\nPlease select a correct account to proceed.\n";
                return 0;
            }
            return accountNum;
        }

        private Int32 getSelectedToAccount()
        {
            string selectedToAccntNum = toAccount.SelectedValue;
            Int32 accountNum = 0;
            try
            {
                accountNum = Int32.Parse(selectedToAccntNum);
                if (selectedToAccntNum.Equals(0))
                {   //error
                    transferErrorLabel.Text = "\nPlease select a correct account to proceed.\n";
                    return 0;
                }
            }
            catch (FormatException parseEx)
            {
                transferErrorLabel.Text = "\nPlease select a correct account to proceed.\n";
                return 0;
            }
            return accountNum;
        }

    }
}