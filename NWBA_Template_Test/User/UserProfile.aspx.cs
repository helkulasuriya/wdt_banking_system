﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using NWBA_Template_Test.App_Code;

namespace NWBA_Template_Test.User
{
    public partial class UserProfile : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session.IsNewSession || ((Session.Keys == null) || (Session.Keys.Count == 0)))
                Response.Redirect("/Auth/Login.aspx");

            if (!IsPostBack)
            {
                //this.Update.Click += new System.EventHandler(this.Update_Button_Click);
                //this.Clear.Click += new System.EventHandler(this.Clear_Button_Click);
                UserFacade userDetails = new UserFacade();
                Customer customer = userDetails.getUserById((int)Session["CustomerId"]);
                if (customer != null)
                {
                    Name.Text = customer.CustomerName;
                    TFN.Text = customer.TFN;
                    Address.Text = customer.Address;
                    City.Text = customer.City;
                    State.Text = customer.State;
                    Postcode.Text = customer.PostCode;
                    Phone.Text = customer.Phone;

                    Password.Text = String.Empty;
                }
                else
                {

                    ErrorLabel.Visible = true;
                }
            }
        }

        protected void Update_Button_Click(object sender, EventArgs e)
        {
            UserFacade userControl = new UserFacade();
            Customer customer = new Customer();
            customer.CustomerID = (int)Session["CustomerId"];
            customer.CustomerName = Name.Text;

            customer.TFN = TFN.Text;
            customer.Address = Address.Text;
            customer.City = City.Text;
            customer.State = State.Text;
            customer.PostCode = Postcode.Text;
            customer.Phone = Phone.Text;
            string password = Password.Text;
            //how can the password be checked
            Updation.Text = userControl.updateUser(customer, password);
            Updation.Visible = true;
        }

        protected void Clear_Button_Click(object sender, EventArgs e)
        {
            UserFacade userDetails = new UserFacade();
            Customer customer = userDetails.getUserById((int)Session["CustomerId"]);
            if (customer != null)
            {
                Name.Text = customer.CustomerName;
                TFN.Text = customer.TFN;
                Address.Text = customer.Address;
                City.Text = customer.City;
                State.Text = customer.State;
                Postcode.Text = customer.PostCode;
                Phone.Text = customer.Phone;
            }
            else
            {
                ErrorLabel.Visible = true;
            }
        }
    }
}