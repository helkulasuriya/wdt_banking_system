﻿<%@ Page Title="" Language="C#" MasterPageFile="User.Master" AutoEventWireup="true" CodeBehind="CreateAccount.aspx.cs" Inherits="NWBA_Template_Test.User.CreateAccount" %>

<asp:Content ID="Content3" ContentPlaceHolderID="MainContent" runat="server">
    <script type="text/javascript">
        
    </script>

    <asp:Panel ID="Panel1" runat="server" HorizontalAlign="Left">
        <div>

            <form ID="createAccountForm" action="ATM.aspx" method="post">
                <asp:Table ID="Table1" runat="server" Height="178px" Width="426px">
                    <asp:TableRow ID="TableRow4" runat="server">
                        <asp:TableCell ID="cell6" runat="server">
                            <asp:Label ID="errorLabel" runat="server" ForeColor="Red"></asp:Label>
                        </asp:TableCell>
                    </asp:TableRow>
                    <asp:TableRow ID="row1" runat="server">
                        <asp:TableCell ID="cell1" runat="server" HorizontalAlign="Left" VerticalAlign="Middle">
                            <asp:Label ID="Label1" runat="server" Text="Choose Account Type" AssociatedControlID="selectAccountType"></asp:Label>
                        </asp:TableCell>
                        <asp:TableCell ID="cell2" runat="server" HorizontalAlign="Left" VerticalAlign="Middle">
                            <asp:DropDownList ID="selectAccountType" runat="server" AutoPostBack="True">
                                <asp:ListItem Value="None" Selected="True">None..</asp:ListItem>
                                <asp:ListItem Value="S">Savings Account</asp:ListItem>
                                <asp:ListItem Value="C">Checking Account</asp:ListItem>
                            </asp:DropDownList>
                        </asp:TableCell>
                    </asp:TableRow>
                    <asp:TableRow ID="row2" runat="server">
                        <asp:TableCell ID="cell3" runat="server" HorizontalAlign="Left" VerticalAlign="Middle">
                            <asp:Label ID="Label2" runat="server" Text="Insert Deposit Amount" AssociatedControlID="depositAmtBox"></asp:Label>
                        </asp:TableCell>
                        <asp:TableCell ID="cell4" runat="server" HorizontalAlign="Left" VerticalAlign="Middle">
                            <asp:TextBox ID="depositAmtBox" runat="server" MaxLength="11"></asp:TextBox>
                            <br />
                            <asp:RequiredFieldValidator ID="AmtRequiredFieldValidator1" runat="server" ErrorMessage="Deposit amount is required." ControlToValidate="depositAmtBox" Font-Bold="False" Font-Italic="True"></asp:RequiredFieldValidator>
                            <asp:RegularExpressionValidator ID="DepositAmoutValidator" runat="server" ErrorMessage="Check the entered amount!" ValidationExpression="\d{3,8}(.\d{1,2})?" ControlToValidate="depositAmtBox"></asp:RegularExpressionValidator>
                            <asp:RangeValidator ID="DepositRangeValidator" runat="server" ErrorMessage="Check the entered amount!" ControlToValidate="depositAmtBox" MinimumValue="100" Type="Double" MaximumValue="99999999.99"></asp:RangeValidator>
                             </asp:TableCell>
                    </asp:TableRow>
                    <asp:TableRow ID="row3" runat="server">
                        <asp:TableCell ID="cell5" runat="server">
                            <asp:Button ID="creatAccountBtn" runat="server" Text="Create Account" Enabled="True" OnClick="creatAccountBtn_Click" />
                        </asp:TableCell>
                    </asp:TableRow>
                </asp:Table>
            </form>
            
        </div>
    </asp:Panel>
    </asp:Content>
