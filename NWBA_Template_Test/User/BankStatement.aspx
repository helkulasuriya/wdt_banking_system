﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="BankStatement.aspx.cs" Inherits="NWBA_Template_Test.User.BankStatement" MasterPageFile="User.Master" %>

<asp:Content runat="server" ID="BodyContent" ContentPlaceHolderID="MainContent">

    <asp:Panel ID="Panel1" runat="server" CssClass="text" HorizontalAlign="Left">
        <asp:Label ID="Label3" runat="server" Text="Want to open a new acccount ! " Font-Bold="True"></asp:Label>
        <asp:HyperLink ID="createAccntLink" runat="server" NavigateUrl="~/User/CreateAccount.aspx" Font-Size="Large">Click here..</asp:HyperLink><br />
        <br />
        <br />

        <asp:Label ID="Label2" runat="server" Text="Choose Account Type" AssociatedControlID="selectAccountType"></asp:Label>
        <asp:DropDownList ID="selectAccountType" runat="server" OnSelectedIndexChanged="selectAccountType_SelectedIndexChanged" AutoPostBack="True">
            <asp:ListItem Value="None" Selected="True">None..</asp:ListItem>
            <asp:ListItem Value="S">Savings Account</asp:ListItem>
            <asp:ListItem Value="C">Checking Account</asp:ListItem>
        </asp:DropDownList>
        <br />
        <br />

        <asp:Label ID="Label1" runat="server" Text="Choose Account to View History" AssociatedControlID="chooseAccount"></asp:Label>
        <asp:DropDownList ID="chooseAccount" runat="server" OnSelectedIndexChanged="chooseAccount_SelectedIndexChanged" AutoPostBack="True">
            <asp:ListItem></asp:ListItem>
        </asp:DropDownList>
        <br />
        <br />
        <br />

        <div style="width: 500px;">
            <fieldset>
                <br />
                <legend>Current Balance</legend>
                <asp:Table ID="Table1" runat="server">
                    <asp:TableRow ID="tabRow" runat="server">
                        <asp:TableCell ID="lableCell" runat="server" HorizontalAlign="Left" VerticalAlign="Middle">
                            <asp:Label ID="balanceLabelText" runat="server" Text="Current Balance of " AssociatedControlID="balanceAmtLabel" Style="float: left"></asp:Label>
                        </asp:TableCell>
                        <asp:TableCell ID="valueCell" runat="server" HorizontalAlign="Center" VerticalAlign="Middle">
                            <asp:Label ID="balanceAmtLabel" runat="server" Style="float: left"></asp:Label>
                        </asp:TableCell>
                    </asp:TableRow>
                </asp:Table>
                <br />
            </fieldset>
            <br />
            <br />
            <br />

            <asp:Button ID="viewAcntHistory" runat="server" OnClick="viewAcntHistory_Click" Text="I wish to see the transaction history." Visible="False" />
            <br />
            <br />
            <br />
        </div>

        <div id="historyGridDiv">
            <asp:GridView ID="accountHistoryGrid" runat="server" AllowPaging="True" BackColor="White" BorderColor="#E7E7FF" BorderStyle="None" BorderWidth="1px" CellPadding="3" GridLines="Horizontal" PageSize="4" OnPageIndexChanging="accountHistoryGrid_PageIndexChanging">
                <AlternatingRowStyle BackColor="#F7F7F7" />
                <FooterStyle BackColor="#B5C7DE" ForeColor="#4A3C8C" />
                <HeaderStyle BackColor="#4A3C8C" Font-Bold="True" ForeColor="#F7F7F7" />
                <PagerStyle BackColor="#E7E7FF" ForeColor="#4A3C8C" HorizontalAlign="Right" />
                <RowStyle BackColor="#E7E7FF" ForeColor="#4A3C8C" />
                <SelectedRowStyle BackColor="#738A9C" Font-Bold="True" ForeColor="#F7F7F7" />
                <SortedAscendingCellStyle BackColor="#F4F4FD" />
                <SortedAscendingHeaderStyle BackColor="#5A4C9D" />
                <SortedDescendingCellStyle BackColor="#D8D8F0" />
                <SortedDescendingHeaderStyle BackColor="#3E3277" />
            </asp:GridView>
            <br />
        </div>
    </asp:Panel>

</asp:Content>
