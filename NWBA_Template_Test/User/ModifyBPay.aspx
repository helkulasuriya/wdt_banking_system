﻿<%@ Page Title="" Language="C#" MasterPageFile="~/User/User.Master" AutoEventWireup="true" CodeBehind="ModifyBPay.aspx.cs" Inherits="NWBA_Template_Test.User.ModifyBPay" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">

    <div id="content">
        <asp:Panel ID="Panel1" runat="server">

            <fieldset>
                <legend>Schedule a Bill Payment</legend>
                <div style="float: left;">
                    <form id="bPayForm" action="BPayList.aspx" method="post">
                        <ol>
                            <li>
                                <asp:Label ID="errorLabel" runat="server" Font-Italic="True" ForeColor="Red"></asp:Label>
                            </li>
                            <li>
                                <asp:Label ID="Label7" runat="server" AssociatedControlID="fromAccountDropBox">From Account</asp:Label>
                                <asp:DropDownList ID="fromAccountDropBox" runat="server" AutoPostBack="True">
                                </asp:DropDownList>
                            </li>
                            <li>
                                <asp:Label ID="Label1" runat="server" AssociatedControlID="PayeesDropBox">To Payee</asp:Label>
                                <asp:DropDownList ID="PayeesDropBox" runat="server"></asp:DropDownList>
                            </li>
                            <li>
                                <asp:Label ID="Label2" runat="server" AssociatedControlID="BillPayAmount">Amount</asp:Label>
                                <asp:TextBox ID="BillPayAmount" runat="server"></asp:TextBox>
                                <asp:RegularExpressionValidator ID="amtExpressionValidator" runat="server" ErrorMessage="Please insert a correct currency value." Display="Dynamic" EnableClientScript="True" ControlToValidate="BillPayAmount" ForeColor="Red" ValidationExpression="[0-9]+(.[0-9]{1,2})?$"></asp:RegularExpressionValidator>
                            </li>
                            <li>
                                <asp:Label ID="Label3" runat="server" AssociatedControlID="ScheduleDateCalendar">Scheduled Date and Time</asp:Label>
                                <asp:Calendar ID="ScheduleDateCalendar" runat="server">
                                    <TodayDayStyle BackColor="#33CCFF" />
                                </asp:Calendar>
                                <br />
                                <asp:TextBox ID="HoursText" runat="server" MaxLength="2" TextMode="Number" ToolTip="Please insert hours in 24 hour format."></asp:TextBox>
                                <asp:RangeValidator ID="hourRangeValidator" runat="server" ErrorMessage="Hours should be between 0 - 24" ControlToValidate="HoursText" Display="Dynamic" ForeColor="Red" MaximumValue="24" MinimumValue="0"></asp:RangeValidator>
                                : 
                                <asp:TextBox ID="MinutesText" runat="server" MaxLength="2" TextMode="Number" ToolTip="Please insert minutes here."></asp:TextBox>
                                <asp:RangeValidator ID="minuteRangeValidator" runat="server" ErrorMessage="Minutes should be between 0 - 59" ControlToValidate="MinutesText" Display="Dynamic" ForeColor="Red" MaximumValue="59" MinimumValue="0"></asp:RangeValidator>
                            </li>
                            <li>
                                <asp:Label ID="Label4" runat="server" AssociatedControlID="PeriodDropBox">Period</asp:Label>
                                <asp:DropDownList ID="PeriodDropBox" runat="server">
                                    <asp:ListItem Value="d">Daily</asp:ListItem>
                                    <asp:ListItem Value="w">Weekly</asp:ListItem>
                                    <asp:ListItem Value="m">Monthly</asp:ListItem>
                                    <asp:ListItem Value="q">Quarterly</asp:ListItem>
                                    <asp:ListItem Value="a">Annually</asp:ListItem>
                                    <asp:ListItem Value="o">Once Off</asp:ListItem>
                                </asp:DropDownList>
                            </li>
                            <li>
                                <br />
                                <asp:Button ID="updateBPayBtn" runat="server" Text="Modify Bill Payment" OnClick="updateBPayBtn_Click" />
                            </li>
                        </ol>
                    </form>
                </div>
            </fieldset>

        </asp:Panel>
    </div>

</asp:Content>
