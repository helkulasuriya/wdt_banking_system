﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using NWBA_Template_Test.App_Code;

namespace NWBA_Template_Test.User
{
    public partial class BPay : System.Web.UI.Page
    {
        private ITransactionFacade transactionFacade = new TransactionFacade();
        private IBPayFacade bPayFacade = new BPayFacade();

        protected void Page_Load(object sender, EventArgs e)
        {
            // Check for session expiration.
            if (Session.IsNewSession || ((Session.Keys == null) || (Session.Keys.Count == 0)))
                Response.Redirect("/Auth/Login.aspx");

            if (!IsPostBack)
            {
                populateAccountDropBox();
                populatePayeeDropBox();
            }
        }

        private void populateAccountDropBox()
        {
            Int32 customerId = (Int32)Session["CustomerId"];
            List<Account> savingsAccounts = transactionFacade.getAccountsByUserandType(customerId, "S");
            List<Account> checkingAccounts = transactionFacade.getAccountsByUserandType(customerId, "C");
            int counter = 1;

            fromAccountDropBox.Items.Clear();
            fromAccountDropBox.Items.Add("None..");
            fromAccountDropBox.SelectedIndex = 0;

            if ((savingsAccounts != null) && !(savingsAccounts.Count() == 0))
            {
                foreach (Account accnt in savingsAccounts)
                {
                    String item = "Savings";
                    if (accnt != null)
                    {
                        item += " - " + accnt.AccountNumber + " -   AUS$ " + transactionFacade.showBalance(accnt.AccountNumber);
                    }
                    fromAccountDropBox.Items.Insert(counter++, new ListItem(item, (accnt.AccountNumber).ToString()));

                }
            }
            if ((checkingAccounts != null) && !(checkingAccounts.Count() == 0))
            {
                foreach (Account accnt in checkingAccounts)
                {
                    String item = "Checking";
                    if (accnt != null)
                    {
                        item += " - " + accnt.AccountNumber + " -   AUS$ " + transactionFacade.showBalance(accnt.AccountNumber);

                    }
                    fromAccountDropBox.Items.Insert(counter++, new ListItem(item, (accnt.AccountNumber).ToString()));
                }
            }
        }

        private void populatePayeeDropBox()
        {
            List<Payee> payeesList = bPayFacade.getAllPayeesList();
            int counter = 0;

            if ((payeesList != null) && !(payeesList.Count() == 0))
            {
                foreach (Payee payee in payeesList)
                {
                    PayeesDropBox.Items.Insert(counter++, new ListItem(payee.PayeeName, (payee.PayeeID).ToString()));
                }
            }
        }

        private DateTime getInsertedScheduleDateTime()
        {
            int hours = 0, minutes = 0;
            try
            {
                hours = Int32.Parse(HoursText.Text);
                minutes = Int32.Parse(MinutesText.Text);
            }
            catch (Exception ex)
            {
                errorLabel.Text = "Please insert correct hours and minute values.";
                return DateTime.Now;
            }
            // Assembling the values to form the DateTime object
            DateTime insertedDate = new DateTime(ScheduleDateCalendar.SelectedDate.Year,
                ScheduleDateCalendar.SelectedDate.Month, ScheduleDateCalendar.SelectedDate.Day,
                hours, minutes, 0);

            return insertedDate;
        }
                
        protected void billPayBtn_Click(object sender, EventArgs e)
        {
            BillPay billPay = new BillPay();
            try
            {
                billPay.AccountNumber = Int32.Parse(fromAccountDropBox.SelectedValue);
                billPay.PayeeID = Int32.Parse(PayeesDropBox.SelectedValue);
                billPay.Amount = Decimal.Parse(BillPayAmount.Text);

                if (getInsertedScheduleDateTime().Equals(DateTime.Now))
                    return;
                else
                    billPay.ScheduleDate = getInsertedScheduleDateTime();

                billPay.Period = PeriodDropBox.SelectedValue;
                billPay.RecurringStatus = "active";
                billPay.ModifyDate = DateTime.Now;

                if (bPayFacade.addBillPayRecord(billPay) == null)
                {
                    errorLabel.Text = "Error in inserting data";
                    return;
                }
                else
                    Response.Redirect("/User/BPayList.aspx");

            }
            catch (Exception ex)
            {
                errorLabel.Text = "Error in inserted values. Please provide the proper input values.";
                return;
            }
        }        

    }
}