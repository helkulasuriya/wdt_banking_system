﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using NWBA_Template_Test.App_Code;

namespace NWBA_Template_Test.User
{
    public partial class BPayList : System.Web.UI.Page
    {
        private IBPayFacade bPayDAO = new BPayFacade();
        static List<BillPay> billPayList = null;

        protected void Page_Load(object sender, EventArgs e)
        {
            // Check for session expiration.
            if (Session.IsNewSession || ((Session.Keys == null) || (Session.Keys.Count == 0)))
                Response.Redirect("/Auth/Login.aspx");

            bindDataToGrid();

        }

        private void bindDataToGrid()
        {
            //billPayList = bPayDAO.getBillPaysWithPayeeName();
            billPayList = bPayDAO.getAllBillPaysList((Int32)Session["CustomerId"]);

            BPayGridView.DataSource = billPayList;
            BPayGridView.DataBind();
        }

        protected void BPayGridView_SelectedIndexChanged(object sender, EventArgs e)
        {
            // Retrieving the selected bill pay ID and adding it to the session.
            Int32 selectedIndex = BPayGridView.SelectedRow.DataItemIndex;
            BillPay billPayID = billPayList[selectedIndex];
            Session["BPayObj"] = billPayID;
            Session["RedirectedFromBPayList"] = true;
            // Redirecting the user to the modify screen.
            Response.Redirect("/User/ModifyBPay.aspx");
        }

        public bool isFailedOrDone(String recurringStatus)
        {
            return ((recurringStatus != "failed") && (recurringStatus != "done"));
        }

        protected void BPayGridView_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if ((e.Row.Cells[4].Text == "failed") || (e.Row.Cells[4].Text == "inactive") || (e.Row.Cells[4].Text == "done"))
                e.Row.Cells[5].Visible = false;
        }
      
    }
}