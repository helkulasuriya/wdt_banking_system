﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="NormalUserNavControl.ascx.cs" Inherits="NWBA_Template_Test.User.NormalUserNavControl" %>

<header>
        <div class="menue_div_top">
            <table>
                <tbody>
                    <tr>
                        <td>
                            <img id="avc_logo_image" src="/Images/bankLogo2.png" width="80" height="80" style="padding-left: 50px; border: 0px" />
                        </td>
                        <td>
                            <div class="menue_div">
                                <ul class="Superfish menu">                                    
                                    <li class="selected current tab">
                                        <a class="menu_link" href="/User/ATM.aspx">ATM</a>
                                    </li>
                                    <li class="selected current tab">
                                        <a class="menu_link" href="/User/BankStatement.aspx">My Statement</a>
                                    </li>
                                    <li class="selected current tab">
                                        <a class="menu_link" href="/User/UserProfile.aspx">My Profile</a>
                                    </li>
                                    <li class="selected current tab">
                                        <a class="menu_link" href="/User/BPay.aspx">Bill Pay</a>
                                    </li>
                                    <li class="selected current tab">
                                        <a class="menu_link" href="/Auth/Logout.aspx">Logout</a>
                                    </li>
                                </ul>
                            </div>
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
    </header>