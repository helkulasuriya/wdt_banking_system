﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using NWBA_Template_Test.App_Code;

namespace NWBA_Template_Test.User
{
    public partial class CreateAccount : System.Web.UI.Page
    {
        private ITransactionFacade transactionFacade = new TransactionFacade();

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session.IsNewSession || ((Session.Keys == null) || (Session.Keys.Count == 0)))
                Response.Redirect("/Auth/Login.aspx");            
        }

        protected void creatAccountBtn_Click(object sender, EventArgs e)
        {
            Int32 customerId = (Int32)Session["CustomerId"];
            //Int32 customerId = 1;
            String accntType = selectAccountType.SelectedValue;
            Decimal depositAmt = Convert.ToDecimal(0.0);
            try
            {
                depositAmt = Convert.ToDecimal(depositAmtBox.Text);
            }
            catch (Exception numFormatEx)
            {
                errorLabel.Text = "Error in parsing the deposit amount.";
                Trace.Write("\n Error in parsing the deposit amount. \n" + numFormatEx.StackTrace);
            }

            if (!accntType.Equals("S") && !accntType.Equals("C"))
                errorLabel.Text = "\n Please select a correct account type. \n";
            else if ((customerId != 0) && (depositAmt != 0.0M))
            {
                // Calling insert account function and redirecting the user accordingly.
                SortedList<String, Boolean> statusMap = transactionFacade.insertAccount(depositAmt, accntType, customerId);

                if (statusMap.Values[0].Equals(true))
                    Response.Redirect("/User/BankStatement.aspx");
                else
                    errorLabel.Text = statusMap.Keys[0];
            }
        }

    }
}