﻿<%@ Page Title="" Language="C#" MasterPageFile="~/User/User.Master" AutoEventWireup="true" CodeBehind="BPayList.aspx.cs" Inherits="NWBA_Template_Test.User.BPayList" EnableEventValidation="true" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">

    <div id="content">
        <asp:Panel ID="Panel1" runat="server">

            <asp:GridView ID="BPayGridView" runat="server" AutoGenerateColumns="False" OnSelectedIndexChanged="BPayGridView_SelectedIndexChanged" OnRowDataBound="BPayGridView_RowDataBound">
                <Columns>
                    <asp:BoundField
                        DataField="Payee.PayeeName" HeaderText="Payee Name"
                        ReadOnly="True" />

                    <asp:BoundField
                        DataField="Amount" HeaderText="Amount"
                        ReadOnly="True" />

                    <asp:BoundField
                        DataField="ScheduleDate" HeaderText="Schedule Date"
                        ReadOnly="True" />

                    <asp:TemplateField HeaderText="Period">
                        <ItemTemplate>
                            <asp:DropDownList ID="PeriodDropBox" runat="server" SelectedValue='<%# Eval("Period")%>' Enabled="false">
                                <asp:ListItem Value="d">Daily</asp:ListItem>
                                <asp:ListItem Value="w">Weekly</asp:ListItem>
                                <asp:ListItem Value="m">Monthly</asp:ListItem>
                                <asp:ListItem Value="q">Quarterly</asp:ListItem>
                                <asp:ListItem Value="a">Annually</asp:ListItem>
                                <asp:ListItem Value="o">Once Off</asp:ListItem>
                            </asp:DropDownList>
                        </ItemTemplate>
                    </asp:TemplateField>

                    <asp:BoundField
                        DataField="RecurringStatus" HeaderText="Status"
                        ReadOnly="True" />

                    <asp:ButtonField ButtonType="Button"
                        CommandName="Select"
                        HeaderText="Modify Bill Payment"
                        Text="Modify" />

                </Columns>
            </asp:GridView>

        </asp:Panel>
    </div>
</asp:Content>
