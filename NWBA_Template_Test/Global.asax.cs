﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.SessionState;
using System.Threading;

using NWBA_Template_Test.App_Code;

namespace NWBA_Template_Test
{
    public class Global : System.Web.HttpApplication
    {
        public Thread globalSchedularThread = null;
        private IBPayFacade bPayFacade = new BPayFacade();

        protected void Application_Start(object sender, EventArgs e)
        {

        }

        protected void Session_Start(object sender, EventArgs e)
        {
            globalSchedularThread = new Thread(new ThreadStart(bPayFacade.scheduleRecurringPayment));
            globalSchedularThread.Priority = ThreadPriority.AboveNormal;
            globalSchedularThread.IsBackground = true;
            globalSchedularThread.Start();
        }

        protected void Application_BeginRequest(object sender, EventArgs e)
        {

        }

        protected void Application_AuthenticateRequest(object sender, EventArgs e)
        {

        }

        protected void Application_Error(object sender, EventArgs e)
        {

        }

        protected void Session_End(object sender, EventArgs e)
        {

        }

        protected void Application_End(object sender, EventArgs e)
        {

        }
    }
}