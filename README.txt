================
WDT Assignment 2
================

Simulated banking web application developed using ASP.NET Web Forms and,
C#.NET and SQL Server back-end. Application includes features such as, 
    checking balance, 
    modify a personal profile,
    simulate transactions such as deposits and withdrawals,
    schedule payments and,
    perform some administrative tasks (Only the authorised user).

Team Members:
Ayush Agrawal 	  - s3391854
Helini Kulasuriya - s3395127