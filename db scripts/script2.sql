
-- --------------------------------------------------
-- Entity Designer DDL Script for SQL Server 2005, 2008, and Azure
-- --------------------------------------------------
-- Date Created: 10/01/2013 03:10:37
-- Generated from EDMX file: G:\SEM 3\WDT\Assignment2 docs\NWBA-2013-09-25\NWBA-2013-09-25\NWBA\NWBA\bankDBEntityModel1.edmx
-- --------------------------------------------------

SET QUOTED_IDENTIFIER OFF;
GO
USE [bankDB_WDT1234];
GO
IF SCHEMA_ID(N'dbo') IS NULL EXECUTE(N'CREATE SCHEMA [dbo]');
GO

-- --------------------------------------------------
-- Dropping existing FOREIGN KEY constraints
-- --------------------------------------------------

IF OBJECT_ID(N'[dbo].[FK_Account_Customer]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Account] DROP CONSTRAINT [FK_Account_Customer];
GO
IF OBJECT_ID(N'[dbo].[FK_BillPay_Account]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[BillPay] DROP CONSTRAINT [FK_BillPay_Account];
GO
IF OBJECT_ID(N'[dbo].[FK_BillPay_Payee]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[BillPay] DROP CONSTRAINT [FK_BillPay_Payee];
GO
IF OBJECT_ID(N'[dbo].[FK_Login_Customer]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Login] DROP CONSTRAINT [FK_Login_Customer];
GO
IF OBJECT_ID(N'[dbo].[FK_Transaction_Account]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Transaction] DROP CONSTRAINT [FK_Transaction_Account];
GO
IF OBJECT_ID(N'[dbo].[FK_TransactionDestination_Transaction]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[TransactionDestination] DROP CONSTRAINT [FK_TransactionDestination_Transaction];
GO

-- --------------------------------------------------
-- Dropping existing tables
-- --------------------------------------------------

IF OBJECT_ID(N'[dbo].[Account]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Account];
GO
IF OBJECT_ID(N'[dbo].[BillPay]', 'U') IS NOT NULL
    DROP TABLE [dbo].[BillPay];
GO
IF OBJECT_ID(N'[dbo].[Customer]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Customer];
GO
IF OBJECT_ID(N'[dbo].[Login]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Login];
GO
IF OBJECT_ID(N'[dbo].[Payee]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Payee];
GO
IF OBJECT_ID(N'[dbo].[Transaction]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Transaction];
GO
IF OBJECT_ID(N'[dbo].[TransactionDestination]', 'U') IS NOT NULL
    DROP TABLE [dbo].[TransactionDestination];
GO

-- --------------------------------------------------
-- Creating all tables
-- --------------------------------------------------

-- Creating table 'Accounts'
CREATE TABLE [dbo].[Accounts] (
    [AccountNumber] int IDENTITY(1,1) NOT NULL,
    [AccountType] nvarchar(1)  NOT NULL,
    [CustomerID] int  NOT NULL,
    [ModifyDate] datetime  NOT NULL
);
GO

-- Creating table 'BillPays'
CREATE TABLE [dbo].[BillPays] (
    [BillPayID] int IDENTITY(1,1) NOT NULL,
    [AccountNumber] int  NOT NULL,
    [PayeeID] int  NOT NULL,
    [Amount] decimal(19,4)  NOT NULL,
    [ScheduleDate] datetime  NOT NULL,
    [Period] nvarchar(1)  NOT NULL,
    [ModifyDate] datetime  NOT NULL
);
GO

-- Creating table 'Customers'
CREATE TABLE [dbo].[Customers] (
    [CustomerID] int IDENTITY(1,1) NOT NULL,
    [CustomerName] nvarchar(50)  NOT NULL,
    [TFN] nvarchar(11)  NULL,
    [Address] nvarchar(50)  NULL,
    [City] nvarchar(40)  NULL,
    [State] varchar(20)  NULL,
    [PostCode] nvarchar(10)  NULL,
    [Phone] nvarchar(15)  NOT NULL
);
GO

-- Creating table 'Logins'
CREATE TABLE [dbo].[Logins] (
    [CustomerID] int IDENTITY(1,1) NOT NULL,
    [UserID] nvarchar(50)  NOT NULL,
    [Password] nvarchar(max)  NOT NULL,
    [ModifyDate] datetime  NOT NULL
);
GO

-- Creating table 'Payees'
CREATE TABLE [dbo].[Payees] (
    [PayeeID] int IDENTITY(1,1) NOT NULL,
    [PayeeName] nvarchar(50)  NOT NULL,
    [Address] nvarchar(50)  NULL,
    [City] nvarchar(40)  NULL,
    [State] nvarchar(20)  NULL,
    [PostCode] nvarchar(10)  NULL,
    [Phone] nvarchar(15)  NOT NULL
);
GO

-- Creating table 'Transactions'
CREATE TABLE [dbo].[Transactions] (
    [TransactionID] int IDENTITY(1,1) NOT NULL,
    [TransactionType] nvarchar(1)  NOT NULL,
    [SourceAccount] int  NOT NULL,
    [Amount] decimal(19,4)  NULL,
    [Comment] nvarchar(255)  NULL,
    [ModifyDate] datetime  NULL
);
GO

-- Creating table 'TransactionDestinations'
CREATE TABLE [dbo].[TransactionDestinations] (
    [TranscationID] int  NOT NULL,
    [DestAccount] int  NOT NULL
);
GO

-- --------------------------------------------------
-- Creating all PRIMARY KEY constraints
-- --------------------------------------------------

-- Creating primary key on [AccountNumber] in table 'Accounts'
ALTER TABLE [dbo].[Accounts]
ADD CONSTRAINT [PK_Accounts]
    PRIMARY KEY CLUSTERED ([AccountNumber] ASC);
GO

-- Creating primary key on [BillPayID] in table 'BillPays'
ALTER TABLE [dbo].[BillPays]
ADD CONSTRAINT [PK_BillPays]
    PRIMARY KEY CLUSTERED ([BillPayID] ASC);
GO

-- Creating primary key on [CustomerID] in table 'Customers'
ALTER TABLE [dbo].[Customers]
ADD CONSTRAINT [PK_Customers]
    PRIMARY KEY CLUSTERED ([CustomerID] ASC);
GO

-- Creating primary key on [CustomerID] in table 'Logins'
ALTER TABLE [dbo].[Logins]
ADD CONSTRAINT [PK_Logins]
    PRIMARY KEY CLUSTERED ([CustomerID] ASC);
GO

-- Creating primary key on [PayeeID] in table 'Payees'
ALTER TABLE [dbo].[Payees]
ADD CONSTRAINT [PK_Payees]
    PRIMARY KEY CLUSTERED ([PayeeID] ASC);
GO

-- Creating primary key on [TransactionID] in table 'Transactions'
ALTER TABLE [dbo].[Transactions]
ADD CONSTRAINT [PK_Transactions]
    PRIMARY KEY CLUSTERED ([TransactionID] ASC);
GO

-- Creating primary key on [TranscationID] in table 'TransactionDestinations'
ALTER TABLE [dbo].[TransactionDestinations]
ADD CONSTRAINT [PK_TransactionDestinations]
    PRIMARY KEY CLUSTERED ([TranscationID] ASC);
GO

-- --------------------------------------------------
-- Creating all FOREIGN KEY constraints
-- --------------------------------------------------

-- Creating foreign key on [CustomerID] in table 'Accounts'
ALTER TABLE [dbo].[Accounts]
ADD CONSTRAINT [FK_Account_Customer]
    FOREIGN KEY ([CustomerID])
    REFERENCES [dbo].[Customers]
        ([CustomerID])
    ON DELETE CASCADE ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_Account_Customer'
CREATE INDEX [IX_FK_Account_Customer]
ON [dbo].[Accounts]
    ([CustomerID]);
GO

-- Creating foreign key on [AccountNumber] in table 'BillPays'
ALTER TABLE [dbo].[BillPays]
ADD CONSTRAINT [FK_BillPay_Account]
    FOREIGN KEY ([AccountNumber])
    REFERENCES [dbo].[Accounts]
        ([AccountNumber])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_BillPay_Account'
CREATE INDEX [IX_FK_BillPay_Account]
ON [dbo].[BillPays]
    ([AccountNumber]);
GO

-- Creating foreign key on [SourceAccount] in table 'Transactions'
ALTER TABLE [dbo].[Transactions]
ADD CONSTRAINT [FK_Transaction_Account]
    FOREIGN KEY ([SourceAccount])
    REFERENCES [dbo].[Accounts]
        ([AccountNumber])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_Transaction_Account'
CREATE INDEX [IX_FK_Transaction_Account]
ON [dbo].[Transactions]
    ([SourceAccount]);
GO

-- Creating foreign key on [PayeeID] in table 'BillPays'
ALTER TABLE [dbo].[BillPays]
ADD CONSTRAINT [FK_BillPay_Payee]
    FOREIGN KEY ([PayeeID])
    REFERENCES [dbo].[Payees]
        ([PayeeID])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_BillPay_Payee'
CREATE INDEX [IX_FK_BillPay_Payee]
ON [dbo].[BillPays]
    ([PayeeID]);
GO

-- Creating foreign key on [CustomerID] in table 'Logins'
ALTER TABLE [dbo].[Logins]
ADD CONSTRAINT [FK_Login_Customer]
    FOREIGN KEY ([CustomerID])
    REFERENCES [dbo].[Customers]
        ([CustomerID])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating foreign key on [TranscationID] in table 'TransactionDestinations'
ALTER TABLE [dbo].[TransactionDestinations]
ADD CONSTRAINT [FK_TransactionDestination_Transaction]
    FOREIGN KEY ([TranscationID])
    REFERENCES [dbo].[Transactions]
        ([TransactionID])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- --------------------------------------------------
-- Script has ended
-- --------------------------------------------------